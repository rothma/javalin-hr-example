package de.ohmhs.bme.database.controller;

import de.ohmhs.bme.database.model.Employee;
import de.ohmhs.bme.database.model.Repository;
import io.javalin.http.Context;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class Controller {

    private final Repository repository;

    public Controller(Repository repository) {
        this.repository = repository;
    }


    public void listEmployees(Context ctx) {
        try {
            List<Employee> emps = repository.findAll();
            ctx.render("template.html", Map.of("employees",emps));
        } catch (SQLException e) {
            ctx.render(e.getMessage());
            e.printStackTrace();
        }
    }
}
