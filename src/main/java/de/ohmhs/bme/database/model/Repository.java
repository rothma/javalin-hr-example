package de.ohmhs.bme.database.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Repository {

    private ConnectionPool pool;

    public Repository() throws SQLException {
        pool = ConnectionPool.getInstance();
    }

    public List<Employee> findAll() throws SQLException {
        Connection con = pool.borrow();
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("select e.last_name, e.first_name, d.department_name " +
                "from employees e " +
                "left outer join departments d on d.department_id = e.department_id");
        List<Employee> emps = new ArrayList<>();
        while (rs.next()) {
            Employee emp = new Employee(
                    rs.getString("LAST_NAME"),
                    rs.getString("FIRST_NAME"));
            if(rs.getString("DEPARTMENT_NAME") != null) {
                emp.setDepartment(new Department(rs.getString("DEPARTMENT_NAME")));
            }
            emps.add(emp);
        }
        rs.close();
        stmt.close();
        pool.release(con);
        return emps;
    }
}
