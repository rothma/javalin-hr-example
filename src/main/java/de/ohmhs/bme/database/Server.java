package de.ohmhs.bme.database;

import de.ohmhs.bme.database.controller.Controller;
import de.ohmhs.bme.database.model.Repository;
import io.javalin.Javalin;
import io.javalin.rendering.template.JavalinThymeleaf;

import java.sql.SQLException;

import static org.eclipse.jetty.util.component.LifeCycle.start;

public class Server {
    public static void main(String[] args) {
        Javalin app = Javalin.create(config -> {
            config.fileRenderer(new JavalinThymeleaf());
        });
        try {
            Repository repository = new Repository();
            Controller controller = new Controller(repository);
            app.get("/employees", controller::listEmployees);
            app.start(8080);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
