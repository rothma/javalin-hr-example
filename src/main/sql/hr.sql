--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: hr; Type: SCHEMA; Schema: -; Owner: hr
--

CREATE SCHEMA hr;

ALTER SCHEMA hr OWNER TO hr;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: countries; Type: TABLE; Schema: hr; Owner: hr
--

CREATE TABLE hr.countries (
    country_id character(2) NOT NULL,
    country_name character varying(40),
    region_id numeric
);


ALTER TABLE hr.countries OWNER TO hr;

--
-- Name: COLUMN countries.country_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.countries.country_id IS 'Primary key of countries table.';


--
-- Name: COLUMN countries.country_name; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.countries.country_name IS 'Country name';


--
-- Name: COLUMN countries.region_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.countries.region_id IS 'Region ID for the country. Foreign key to region_id column in the departments table.';


--
-- Name: departments; Type: TABLE; Schema: hr; Owner: hr
--

CREATE TABLE hr.departments (
    department_id numeric(4,0) NOT NULL,
    department_name character varying(30) NOT NULL,
    manager_id numeric(6,0),
    location_id numeric(4,0)
);


ALTER TABLE hr.departments OWNER TO hr;

--
-- Name: COLUMN departments.department_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.departments.department_id IS 'Primary key column of departments table.';


--
-- Name: COLUMN departments.department_name; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.departments.department_name IS 'A not null column that shows name of a department. Administration,
Marketing, Purchasing, Human Resources, Shipping, IT, Executive, Public
Relations, Sales, Finance, and Accounting. ';


--
-- Name: COLUMN departments.manager_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.departments.manager_id IS 'Manager_id of a department. Foreign key to employee_id column of employees table. The manager_id column of the employee table references this column.';


--
-- Name: COLUMN departments.location_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.departments.location_id IS 'Location id where a department is located. Foreign key to location_id column of locations table.';


--
-- Name: departments_seq; Type: SEQUENCE; Schema: hr; Owner: hr
--

CREATE SEQUENCE hr.departments_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9990
    CACHE 1;


ALTER TABLE hr.departments_seq OWNER TO hr;

--
-- Name: employees; Type: TABLE; Schema: hr; Owner: hr
--

CREATE TABLE hr.employees (
    employee_id numeric(6,0) NOT NULL,
    first_name character varying(20),
    last_name character varying(25) NOT NULL,
    email character varying(25) NOT NULL,
    phone_number character varying(20),
    hire_date date NOT NULL,
    job_id character varying(10) NOT NULL,
    salary numeric(8,2),
    commission_pct numeric(2,2),
    manager_id numeric(6,0),
    department_id numeric(4,0),
    CONSTRAINT emp_salary_min CHECK ((salary > (0)::numeric))
);


ALTER TABLE hr.employees OWNER TO hr;

--
-- Name: COLUMN employees.employee_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.employee_id IS 'Primary key of employees table.';


--
-- Name: COLUMN employees.first_name; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.first_name IS 'First name of the employee. A not null column.';


--
-- Name: COLUMN employees.last_name; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.last_name IS 'Last name of the employee. A not null column.';


--
-- Name: COLUMN employees.email; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.email IS 'Email id of the employee';


--
-- Name: COLUMN employees.phone_number; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.phone_number IS 'Phone number of the employee; includes country code and area code';


--
-- Name: COLUMN employees.hire_date; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.hire_date IS 'Date when the employee started on this job. A not null column.';


--
-- Name: COLUMN employees.job_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.job_id IS 'Current job of the employee; foreign key to job_id column of the
jobs table. A not null column.';


--
-- Name: COLUMN employees.salary; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.salary IS 'Monthly salary of the employee. Must be greater
than zero (enforced by constraint emp_salary_min)';


--
-- Name: COLUMN employees.commission_pct; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.commission_pct IS 'Commission percentage of the employee; Only employees in sales
department elgible for commission percentage';


--
-- Name: COLUMN employees.manager_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.manager_id IS 'Manager id of the employee; has same domain as manager_id in
departments table. Foreign key to employee_id column of employees table.
(useful for reflexive joins and CONNECT BY query)';


--
-- Name: COLUMN employees.department_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.employees.department_id IS 'Department id where employee works; foreign key to department_id
column of the departments table';


--
-- Name: jobs; Type: TABLE; Schema: hr; Owner: hr
--

CREATE TABLE hr.jobs (
    job_id character varying(10) NOT NULL,
    job_title character varying(35) NOT NULL,
    min_salary numeric(6,0),
    max_salary numeric(6,0)
);


ALTER TABLE hr.jobs OWNER TO hr;

--
-- Name: COLUMN jobs.job_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.jobs.job_id IS 'Primary key of jobs table.';


--
-- Name: COLUMN jobs.job_title; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.jobs.job_title IS 'A not null column that shows job title, e.g. AD_VP, FI_ACCOUNTANT';


--
-- Name: COLUMN jobs.min_salary; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.jobs.min_salary IS 'Minimum salary for a job title.';


--
-- Name: COLUMN jobs.max_salary; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.jobs.max_salary IS 'Maximum salary for a job title';


--
-- Name: locations; Type: TABLE; Schema: hr; Owner: hr
--

CREATE TABLE hr.locations (
    location_id numeric(4,0) NOT NULL,
    street_address character varying(40),
    postal_code character varying(12),
    city character varying(30) NOT NULL,
    state_province character varying(25),
    country_id character(2)
);


ALTER TABLE hr.locations OWNER TO hr;

--
-- Name: COLUMN locations.location_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.locations.location_id IS 'Primary key of locations table';


--
-- Name: COLUMN locations.street_address; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.locations.street_address IS 'Street address of an office, warehouse, or production site of a company.
Contains building number and street name';


--
-- Name: COLUMN locations.postal_code; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.locations.postal_code IS 'Postal code of the location of an office, warehouse, or production site
of a company. ';


--
-- Name: COLUMN locations.city; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.locations.city IS 'A not null column that shows city where an office, warehouse, or
production site of a company is located. ';


--
-- Name: COLUMN locations.state_province; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.locations.state_province IS 'State or Province where an office, warehouse, or production site of a
company is located.';


--
-- Name: COLUMN locations.country_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.locations.country_id IS 'Country where an office, warehouse, or production site of a company is
located. Foreign key to country_id column of the countries table.';


--
-- Name: regions; Type: TABLE; Schema: hr; Owner: hr
--

CREATE TABLE hr.regions (
    region_id numeric NOT NULL,
    region_name character varying(25)
);


ALTER TABLE hr.regions OWNER TO hr;

--
-- Name: emp_details_view; Type: VIEW; Schema: hr; Owner: hr
--

CREATE VIEW hr.emp_details_view AS
 SELECT e.employee_id,
    e.job_id,
    e.manager_id,
    e.department_id,
    d.location_id,
    l.country_id,
    e.first_name,
    e.last_name,
    e.salary,
    e.commission_pct,
    d.department_name,
    j.job_title,
    l.city,
    l.state_province,
    c.country_name,
    r.region_name
   FROM hr.employees e,
    hr.departments d,
    hr.jobs j,
    hr.locations l,
    hr.countries c,
    hr.regions r
  WHERE ((e.department_id = d.department_id) AND (d.location_id = l.location_id) AND (l.country_id = c.country_id) AND (c.region_id = r.region_id) AND ((j.job_id)::text = (e.job_id)::text));


ALTER TABLE hr.emp_details_view OWNER TO hr;

--
-- Name: employees_seq; Type: SEQUENCE; Schema: hr; Owner: hr
--

CREATE SEQUENCE hr.employees_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hr.employees_seq OWNER TO hr;

--
-- Name: job_history; Type: TABLE; Schema: hr; Owner: hr
--

CREATE TABLE hr.job_history (
    employee_id numeric(6,0) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    job_id character varying(10) NOT NULL,
    department_id numeric(4,0),
    CONSTRAINT jhist_date_check CHECK ((end_date > start_date))
);


ALTER TABLE hr.job_history OWNER TO hr;

--
-- Name: COLUMN job_history.employee_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.job_history.employee_id IS 'A not null column in the complex primary key employee_id+start_date.
Foreign key to employee_id column of the employee table';


--
-- Name: COLUMN job_history.start_date; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.job_history.start_date IS 'A not null column in the complex primary key employee_id+start_date.
Must be less than the end_date of the job_history table. (enforced by
constraint jhist_date_interval)';


--
-- Name: COLUMN job_history.end_date; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.job_history.end_date IS 'Last day of the employee in this job role. A not null column. Must be
greater than the start_date of the job_history table.
(enforced by constraint jhist_date_interval)';


--
-- Name: COLUMN job_history.job_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.job_history.job_id IS 'Job role in which the employee worked in the past; foreign key to
job_id column in the jobs table. A not null column.';


--
-- Name: COLUMN job_history.department_id; Type: COMMENT; Schema: hr; Owner: hr
--

COMMENT ON COLUMN hr.job_history.department_id IS 'Department id in which the employee worked in the past; foreign key to deparment_id column in the departments table';


--
-- Name: locations_seq; Type: SEQUENCE; Schema: hr; Owner: hr
--

CREATE SEQUENCE hr.locations_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9900
    CACHE 1;


ALTER TABLE hr.locations_seq OWNER TO hr;

--
-- Data for Name: countries; Type: TABLE DATA; Schema: hr; Owner: hr
--

COPY hr.countries (country_id, country_name, region_id) FROM stdin;
AR	Argentina	2
AU	Australia	3
BE	Belgium	1
BR	Brazil	2
CA	Canada	2
CH	Switzerland	1
CN	China	3
DE	Germany	1
DK	Denmark	1
EG	Egypt	4
FR	France	1
IL	Israel	4
IN	India	3
IT	Italy	1
JP	Japan	3
KW	Kuwait	4
ML	Malaysia	3
MX	Mexico	2
NG	Nigeria	4
NL	Netherlands	1
SG	Singapore	3
UK	United Kingdom	1
US	United States of America	2
ZM	Zambia	4
ZW	Zimbabwe	4
\.


--
-- Data for Name: departments; Type: TABLE DATA; Schema: hr; Owner: hr
--

COPY hr.departments (department_id, department_name, manager_id, location_id) FROM stdin;
10	Administration	200	1700
20	Marketing	201	1800
30	Purchasing	114	1700
40	Human Resources	203	2400
50	Shipping	121	1500
60	IT	103	1400
70	Public Relations	204	2700
80	Sales	145	2500
90	Executive	100	1700
100	Finance	108	1700
110	Accounting	205	1700
120	Treasury	\N	1700
130	Corporate Tax	\N	1700
140	Control And Credit	\N	1700
150	Shareholder Services	\N	1700
160	Benefits	\N	1700
170	Manufacturing	\N	1700
180	Construction	\N	1700
190	Contracting	\N	1700
200	Operations	\N	1700
210	IT Support	\N	1700
220	NOC	\N	1700
230	IT Helpdesk	\N	1700
240	Government Sales	\N	1700
250	Retail Sales	\N	1700
260	Recruiting	\N	1700
270	Payroll	\N	1700
\.


--
-- Data for Name: employees; Type: TABLE DATA; Schema: hr; Owner: hr
--

COPY hr.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, commission_pct, manager_id, department_id) FROM stdin;
101	Neena	Kochhar	NKOCHHAR	515.123.4568	2005-09-21	AD_VP	17000.00	\N	100	90
103	Alexander	Hunold	AHUNOLD	590.423.4567	2006-01-03	IT_PROG	9000.00	\N	102	60
104	Bruce	Ernst	BERNST	590.423.4568	2007-05-21	IT_PROG	6000.00	\N	103	60
105	David	Austin	DAUSTIN	590.423.4569	2005-06-25	IT_PROG	4800.00	\N	103	60
106	Valli	Pataballa	VPATABAL	590.423.4560	2006-02-05	IT_PROG	4800.00	\N	103	60
107	Diana	Lorentz	DLORENTZ	590.423.5567	2007-02-07	IT_PROG	4200.00	\N	103	60
108	Nancy	Greenberg	NGREENBE	515.124.4569	2002-08-17	FI_MGR	12008.00	\N	101	100
109	Daniel	Faviet	DFAVIET	515.124.4169	2002-08-16	FI_ACCOUNT	9000.00	\N	108	100
110	John	Chen	JCHEN	515.124.4269	2005-09-28	FI_ACCOUNT	8200.00	\N	108	100
111	Ismael	Sciarra	ISCIARRA	515.124.4369	2005-09-30	FI_ACCOUNT	7700.00	\N	108	100
112	Jose Manuel	Urman	JMURMAN	515.124.4469	2006-03-07	FI_ACCOUNT	7800.00	\N	108	100
113	Luis	Popp	LPOPP	515.124.4567	2007-12-07	FI_ACCOUNT	6900.00	\N	108	100
114	Den	Raphaely	DRAPHEAL	515.127.4561	2002-12-07	PU_MAN	11000.00	\N	100	30
115	Alexander	Khoo	AKHOO	515.127.4562	2003-05-18	PU_CLERK	3100.00	\N	114	30
116	Shelli	Baida	SBAIDA	515.127.4563	2005-12-24	PU_CLERK	2900.00	\N	114	30
117	Sigal	Tobias	STOBIAS	515.127.4564	2005-07-24	PU_CLERK	2800.00	\N	114	30
118	Guy	Himuro	GHIMURO	515.127.4565	2006-11-15	PU_CLERK	2600.00	\N	114	30
119	Karen	Colmenares	KCOLMENA	515.127.4566	2007-08-10	PU_CLERK	2500.00	\N	114	30
120	Matthew	Weiss	MWEISS	650.123.1234	2004-07-18	ST_MAN	8000.00	\N	100	50
121	Adam	Fripp	AFRIPP	650.123.2234	2005-04-10	ST_MAN	8200.00	\N	100	50
122	Payam	Kaufling	PKAUFLIN	650.123.3234	2003-05-01	ST_MAN	7900.00	\N	100	50
123	Shanta	Vollman	SVOLLMAN	650.123.4234	2005-10-10	ST_MAN	6500.00	\N	100	50
124	Kevin	Mourgos	KMOURGOS	650.123.5234	2007-11-16	ST_MAN	5800.00	\N	100	50
125	Julia	Nayer	JNAYER	650.124.1214	2005-07-16	ST_CLERK	3200.00	\N	120	50
126	Irene	Mikkilineni	IMIKKILI	650.124.1224	2006-09-28	ST_CLERK	2700.00	\N	120	50
127	James	Landry	JLANDRY	650.124.1334	2007-01-14	ST_CLERK	2400.00	\N	120	50
128	Steven	Markle	SMARKLE	650.124.1434	2008-03-08	ST_CLERK	2200.00	\N	120	50
129	Laura	Bissot	LBISSOT	650.124.5234	2005-08-20	ST_CLERK	3300.00	\N	121	50
130	Mozhe	Atkinson	MATKINSO	650.124.6234	2005-10-30	ST_CLERK	2800.00	\N	121	50
131	James	Marlow	JAMRLOW	650.124.7234	2005-02-16	ST_CLERK	2500.00	\N	121	50
132	TJ	Olson	TJOLSON	650.124.8234	2007-04-10	ST_CLERK	2100.00	\N	121	50
133	Jason	Mallin	JMALLIN	650.127.1934	2004-06-14	ST_CLERK	3300.00	\N	122	50
134	Michael	Rogers	MROGERS	650.127.1834	2006-08-26	ST_CLERK	2900.00	\N	122	50
135	Ki	Gee	KGEE	650.127.1734	2007-12-12	ST_CLERK	2400.00	\N	122	50
136	Hazel	Philtanker	HPHILTAN	650.127.1634	2008-02-06	ST_CLERK	2200.00	\N	122	50
137	Renske	Ladwig	RLADWIG	650.121.1234	2003-07-14	ST_CLERK	3600.00	\N	123	50
138	Stephen	Stiles	SSTILES	650.121.2034	2005-10-26	ST_CLERK	3200.00	\N	123	50
139	John	Seo	JSEO	650.121.2019	2006-02-12	ST_CLERK	2700.00	\N	123	50
140	Joshua	Patel	JPATEL	650.121.1834	2006-04-06	ST_CLERK	2500.00	\N	123	50
141	Trenna	Rajs	TRAJS	650.121.8009	2003-10-17	ST_CLERK	3500.00	\N	124	50
143	Randall	Matos	RMATOS	650.121.2874	2006-03-15	ST_CLERK	2600.00	\N	124	50
144	Peter	Vargas	PVARGAS	650.121.2004	2006-07-09	ST_CLERK	2500.00	\N	124	50
145	John	Russell	JRUSSEL	011.44.1344.429268	2004-10-01	SA_MAN	14000.00	0.40	100	80
146	Karen	Partners	KPARTNER	011.44.1344.467268	2005-01-05	SA_MAN	13500.00	0.30	100	80
147	Alberto	Errazuriz	AERRAZUR	011.44.1344.429278	2005-03-10	SA_MAN	12000.00	0.30	100	80
148	Gerald	Cambrault	GCAMBRAU	011.44.1344.619268	2007-10-15	SA_MAN	11000.00	0.30	100	80
149	Eleni	Zlotkey	EZLOTKEY	011.44.1344.429018	2008-01-29	SA_MAN	10500.00	0.20	100	80
150	Peter	Tucker	PTUCKER	011.44.1344.129268	2005-01-30	SA_REP	10000.00	0.30	145	80
151	David	Bernstein	DBERNSTE	011.44.1344.345268	2005-03-24	SA_REP	9500.00	0.25	145	80
152	Peter	Hall	PHALL	011.44.1344.478968	2005-08-20	SA_REP	9000.00	0.25	145	80
153	Christopher	Olsen	COLSEN	011.44.1344.498718	2006-03-30	SA_REP	8000.00	0.20	145	80
154	Nanette	Cambrault	NCAMBRAU	011.44.1344.987668	2006-12-09	SA_REP	7500.00	0.20	145	80
155	Oliver	Tuvault	OTUVAULT	011.44.1344.486508	2007-11-23	SA_REP	7000.00	0.15	145	80
156	Janette	King	JKING	011.44.1345.429268	2004-01-30	SA_REP	10000.00	0.35	146	80
157	Patrick	Sully	PSULLY	011.44.1345.929268	2004-03-04	SA_REP	9500.00	0.35	146	80
158	Allan	McEwen	AMCEWEN	011.44.1345.829268	2004-08-01	SA_REP	9000.00	0.35	146	80
159	Lindsey	Smith	LSMITH	011.44.1345.729268	2005-03-10	SA_REP	8000.00	0.30	146	80
160	Louise	Doran	LDORAN	011.44.1345.629268	2005-12-15	SA_REP	7500.00	0.30	146	80
161	Sarath	Sewall	SSEWALL	011.44.1345.529268	2006-11-03	SA_REP	7000.00	0.25	146	80
162	Clara	Vishney	CVISHNEY	011.44.1346.129268	2005-11-11	SA_REP	10500.00	0.25	147	80
163	Danielle	Greene	DGREENE	011.44.1346.229268	2007-03-19	SA_REP	9500.00	0.15	147	80
164	Mattea	Marvins	MMARVINS	011.44.1346.329268	2008-01-24	SA_REP	7200.00	0.10	147	80
165	David	Lee	DLEE	011.44.1346.529268	2008-02-23	SA_REP	6800.00	0.10	147	80
166	Sundar	Ande	SANDE	011.44.1346.629268	2008-03-24	SA_REP	6400.00	0.10	147	80
167	Amit	Banda	ABANDA	011.44.1346.729268	2008-04-21	SA_REP	6200.00	0.10	147	80
168	Lisa	Ozer	LOZER	011.44.1343.929268	2005-03-11	SA_REP	11500.00	0.25	148	80
169	Harrison	Bloom	HBLOOM	011.44.1343.829268	2006-03-23	SA_REP	10000.00	0.20	148	80
170	Tayler	Fox	TFOX	011.44.1343.729268	2006-01-24	SA_REP	9600.00	0.20	148	80
171	William	Smith	WSMITH	011.44.1343.629268	2007-02-23	SA_REP	7400.00	0.15	148	80
172	Elizabeth	Bates	EBATES	011.44.1343.529268	2007-03-24	SA_REP	7300.00	0.15	148	80
102	Lex	De Haan	LDEHAAN	515.123.4569	2001-01-13	AD_VP	17000.00	\N	100	90
142	Curtis	Davies	CDAVIES	650.121.2994	2005-01-29	ST_CLERK	3100.00	\N	124	50
173	Sundita	Kumar	SKUMAR	011.44.1343.329268	2008-04-21	SA_REP	6100.00	0.10	148	80
174	Ellen	Abel	EABEL	011.44.1644.429267	2004-05-11	SA_REP	11000.00	0.30	149	80
175	Alyssa	Hutton	AHUTTON	011.44.1644.429266	2005-03-19	SA_REP	8800.00	0.25	149	80
176	Jonathon	Taylor	JTAYLOR	011.44.1644.429265	2006-03-24	SA_REP	8600.00	0.20	149	80
177	Jack	Livingston	JLIVINGS	011.44.1644.429264	2006-04-23	SA_REP	8400.00	0.20	149	80
178	Kimberely	Grant	KGRANT	011.44.1644.429263	2007-05-24	SA_REP	7000.00	0.15	149	\N
179	Charles	Johnson	CJOHNSON	011.44.1644.429262	2008-01-04	SA_REP	6200.00	0.10	149	80
180	Winston	Taylor	WTAYLOR	650.507.9876	2006-01-24	SH_CLERK	3200.00	\N	120	50
181	Jean	Fleaur	JFLEAUR	650.507.9877	2006-02-23	SH_CLERK	3100.00	\N	120	50
182	Martha	Sullivan	MSULLIVA	650.507.9878	2007-06-21	SH_CLERK	2500.00	\N	120	50
183	Girard	Geoni	GGEONI	650.507.9879	2008-02-03	SH_CLERK	2800.00	\N	120	50
184	Nandita	Sarchand	NSARCHAN	650.509.1876	2004-01-27	SH_CLERK	4200.00	\N	121	50
185	Alexis	Bull	ABULL	650.509.2876	2005-02-20	SH_CLERK	4100.00	\N	121	50
186	Julia	Dellinger	JDELLING	650.509.3876	2006-06-24	SH_CLERK	3400.00	\N	121	50
187	Anthony	Cabrio	ACABRIO	650.509.4876	2007-02-07	SH_CLERK	3000.00	\N	121	50
188	Kelly	Chung	KCHUNG	650.505.1876	2005-06-14	SH_CLERK	3800.00	\N	122	50
189	Jennifer	Dilly	JDILLY	650.505.2876	2005-08-13	SH_CLERK	3600.00	\N	122	50
190	Timothy	Gates	TGATES	650.505.3876	2006-07-11	SH_CLERK	2900.00	\N	122	50
191	Randall	Perkins	RPERKINS	650.505.4876	2007-12-19	SH_CLERK	2500.00	\N	122	50
192	Sarah	Bell	SBELL	650.501.1876	2004-02-04	SH_CLERK	4000.00	\N	123	50
193	Britney	Everett	BEVERETT	650.501.2876	2005-03-03	SH_CLERK	3900.00	\N	123	50
194	Samuel	McCain	SMCCAIN	650.501.3876	2006-07-01	SH_CLERK	3200.00	\N	123	50
195	Vance	Jones	VJONES	650.501.4876	2007-03-17	SH_CLERK	2800.00	\N	123	50
196	Alana	Walsh	AWALSH	650.507.9811	2006-04-24	SH_CLERK	3100.00	\N	124	50
197	Kevin	Feeney	KFEENEY	650.507.9822	2006-05-23	SH_CLERK	3000.00	\N	124	50
198	Donald	OConnell	DOCONNEL	650.507.9833	2007-06-21	SH_CLERK	2600.00	\N	124	50
199	Douglas	Grant	DGRANT	650.507.9844	2008-01-13	SH_CLERK	2600.00	\N	124	50
201	Michael	Hartstein	MHARTSTE	515.123.5555	2004-02-17	MK_MAN	13000.00	\N	100	20
202	Pat	Fay	PFAY	603.123.6666	2005-08-17	MK_REP	6000.00	\N	201	20
203	Susan	Mavris	SMAVRIS	515.123.7777	2002-06-07	HR_REP	6500.00	\N	101	40
204	Hermann	Baer	HBAER	515.123.8888	2002-06-07	PR_REP	10000.00	\N	101	70
205	Shelley	Higgins	SHIGGINS	515.123.8080	2002-06-07	AC_MGR	12008.00	\N	101	110
206	William	Gietz	WGIETZ	515.123.8181	2002-06-07	AC_ACCOUNT	8300.00	\N	205	110
100	Steven	King	SKING	515.123.4567	2021-12-09	AD_PRES	24000.00	\N	\N	90
5	John	Doe	JOHNDOE	\N	2021-12-09	SA_MAN	\N	\N	\N	80
200	Neena	Whalen	JWHALEN	515.123.4444	2003-09-17	AD_ASST	4400.00	\N	101	10
\.


--
-- Data for Name: job_history; Type: TABLE DATA; Schema: hr; Owner: hr
--

COPY hr.job_history (employee_id, start_date, end_date, job_id, department_id) FROM stdin;
149	2021-05-28	2021-05-29	AD_ASST	10
149	2020-01-01	2020-12-31	SH_CLERK	50
\.


--
-- Data for Name: jobs; Type: TABLE DATA; Schema: hr; Owner: hr
--

COPY hr.jobs (job_id, job_title, min_salary, max_salary) FROM stdin;
AD_PRES	President	20080	40000
AD_VP	Administration Vice President	15000	30000
AD_ASST	Administration Assistant	3000	6000
FI_MGR	Finance Manager	8200	16000
FI_ACCOUNT	Accountant	4200	9000
AC_MGR	Accounting Manager	8200	16000
AC_ACCOUNT	Public Accountant	4200	9000
SA_MAN	Sales Manager	10000	20080
SA_REP	Sales Representative	6000	12008
PU_MAN	Purchasing Manager	8000	15000
PU_CLERK	Purchasing Clerk	2500	5500
ST_MAN	Stock Manager	5500	8500
ST_CLERK	Stock Clerk	2008	5000
SH_CLERK	Shipping Clerk	2500	5500
IT_PROG	Programmer	4000	10000
MK_MAN	Marketing Manager	9000	15000
MK_REP	Marketing Representative	4000	9000
HR_REP	Human Resources Representative	4000	9000
PR_REP	Public Relations Representative	4500	10500
\.


--
-- Data for Name: locations; Type: TABLE DATA; Schema: hr; Owner: hr
--

COPY hr.locations (location_id, street_address, postal_code, city, state_province, country_id) FROM stdin;
1000	1297 Via Cola di Rie	00989	Roma	\N	IT
1100	93091 Calle della Testa	10934	Venice	\N	IT
1200	2017 Shinjuku-ku	1689	Tokyo	Tokyo Prefecture	JP
1300	9450 Kamiya-cho	6823	Hiroshima	\N	JP
1400	2014 Jabberwocky Rd	26192	Southlake	Texas	US
1500	2011 Interiors Blvd	99236	South San Francisco	California	US
1600	2007 Zagora St	50090	South Brunswick	New Jersey	US
1700	2004 Charade Rd	98199	Seattle	Washington	US
1800	147 Spadina Ave	M5V 2L7	Toronto	Ontario	CA
1900	6092 Boxwood St	YSW 9T2	Whitehorse	Yukon	CA
2000	40-5-12 Laogianggen	190518	Beijing	\N	CN
2100	1298 Vileparle (E)	490231	Bombay	Maharashtra	IN
2200	12-98 Victoria Street	2901	Sydney	New South Wales	AU
2300	198 Clementi North	540198	Singapore	\N	SG
2400	8204 Arthur St	\N	London	\N	UK
2500	Magdalen Centre, The Oxford Science Park	OX9 9ZB	Oxford	Oxford	UK
2600	9702 Chester Road	09629850293	Stretford	Manchester	UK
2700	Schwanthalerstr. 7031	80925	Munich	Bavaria	DE
2800	Rua Frei Caneca 1360 	01307-002	Sao Paulo	Sao Paulo	BR
2900	20 Rue des Corps-Saints	1730	Geneva	Geneve	CH
3000	Murtenstrasse 921	3095	Bern	BE	CH
3100	Pieter Breughelstraat 837	3029SK	Utrecht	Utrecht	NL
3200	Mariano Escobedo 9991	11932	Mexico City	Distrito Federal,	MX
\.


--
-- Data for Name: regions; Type: TABLE DATA; Schema: hr; Owner: hr
--

COPY hr.regions (region_id, region_name) FROM stdin;
1	Europe
2	Americas
3	Asia
4	Middle East and Africa
\.


--
-- Name: departments_seq; Type: SEQUENCE SET; Schema: hr; Owner: hr
--

SELECT pg_catalog.setval('hr.departments_seq', 1, false);


--
-- Name: employees_seq; Type: SEQUENCE SET; Schema: hr; Owner: hr
--

SELECT pg_catalog.setval('hr.employees_seq', 5, true);


--
-- Name: locations_seq; Type: SEQUENCE SET; Schema: hr; Owner: hr
--

SELECT pg_catalog.setval('hr.locations_seq', 1, false);


--
-- Name: countries country_c_id_pk; Type: CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.countries
    ADD CONSTRAINT country_c_id_pk PRIMARY KEY (country_id);


--
-- Name: departments dept_id_pk; Type: CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.departments
    ADD CONSTRAINT dept_id_pk PRIMARY KEY (department_id);


--
-- Name: employees emp_email_uk; Type: CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT emp_email_uk UNIQUE (email);


--
-- Name: employees emp_emp_id_pk; Type: CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT emp_emp_id_pk PRIMARY KEY (employee_id);


--
-- Name: job_history jhist_id_date_pk; Type: CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.job_history
    ADD CONSTRAINT jhist_id_date_pk PRIMARY KEY (employee_id, start_date);


--
-- Name: jobs job_id_pk; Type: CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.jobs
    ADD CONSTRAINT job_id_pk PRIMARY KEY (job_id);


--
-- Name: locations loc_id_pk; Type: CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.locations
    ADD CONSTRAINT loc_id_pk PRIMARY KEY (location_id);


--
-- Name: regions reg_id_pk; Type: CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.regions
    ADD CONSTRAINT reg_id_pk PRIMARY KEY (region_id);


--
-- Name: country_c_id_pkx; Type: INDEX; Schema: hr; Owner: hr
--

CREATE UNIQUE INDEX country_c_id_pkx ON hr.countries USING btree (country_id);


--
-- Name: dept_id_pkx; Type: INDEX; Schema: hr; Owner: hr
--

CREATE UNIQUE INDEX dept_id_pkx ON hr.departments USING btree (department_id);


--
-- Name: dept_location_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX dept_location_ix ON hr.departments USING btree (location_id);


--
-- Name: emp_department_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX emp_department_ix ON hr.employees USING btree (department_id);


--
-- Name: emp_emp_id_pkx; Type: INDEX; Schema: hr; Owner: hr
--

CREATE UNIQUE INDEX emp_emp_id_pkx ON hr.employees USING btree (employee_id);


--
-- Name: emp_job_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX emp_job_ix ON hr.employees USING btree (job_id);


--
-- Name: emp_manager_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX emp_manager_ix ON hr.employees USING btree (manager_id);


--
-- Name: emp_name_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX emp_name_ix ON hr.employees USING btree (last_name, first_name);


--
-- Name: jhist_dept_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX jhist_dept_ix ON hr.job_history USING btree (department_id);


--
-- Name: jhist_emp_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX jhist_emp_ix ON hr.job_history USING btree (employee_id);


--
-- Name: jhist_id_date_pkx; Type: INDEX; Schema: hr; Owner: hr
--

CREATE UNIQUE INDEX jhist_id_date_pkx ON hr.job_history USING btree (employee_id, start_date);


--
-- Name: jhist_job_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX jhist_job_ix ON hr.job_history USING btree (job_id);


--
-- Name: job_id_pkx; Type: INDEX; Schema: hr; Owner: hr
--

CREATE UNIQUE INDEX job_id_pkx ON hr.jobs USING btree (job_id);


--
-- Name: loc_city_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX loc_city_ix ON hr.locations USING btree (city);


--
-- Name: loc_country_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX loc_country_ix ON hr.locations USING btree (country_id);


--
-- Name: loc_id_pkx; Type: INDEX; Schema: hr; Owner: hr
--

CREATE UNIQUE INDEX loc_id_pkx ON hr.locations USING btree (location_id);


--
-- Name: loc_state_prov_ix; Type: INDEX; Schema: hr; Owner: hr
--

CREATE INDEX loc_state_prov_ix ON hr.locations USING btree (state_province);


--
-- Name: reg_id_pkx; Type: INDEX; Schema: hr; Owner: hr
--

CREATE UNIQUE INDEX reg_id_pkx ON hr.regions USING btree (region_id);


--
-- Name: countries countr_reg_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.countries
    ADD CONSTRAINT countr_reg_fk FOREIGN KEY (region_id) REFERENCES hr.regions(region_id);


--
-- Name: departments dept_loc_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.departments
    ADD CONSTRAINT dept_loc_fk FOREIGN KEY (location_id) REFERENCES hr.locations(location_id);


--
-- Name: departments dept_mgr_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.departments
    ADD CONSTRAINT dept_mgr_fk FOREIGN KEY (manager_id) REFERENCES hr.employees(employee_id);


--
-- Name: employees emp_dept_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT emp_dept_fk FOREIGN KEY (department_id) REFERENCES hr.departments(department_id);


--
-- Name: employees emp_job_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT emp_job_fk FOREIGN KEY (job_id) REFERENCES hr.jobs(job_id);


--
-- Name: employees emp_manager_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.employees
    ADD CONSTRAINT emp_manager_fk FOREIGN KEY (manager_id) REFERENCES hr.employees(employee_id);


--
-- Name: job_history jhist_dept_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.job_history
    ADD CONSTRAINT jhist_dept_fk FOREIGN KEY (department_id) REFERENCES hr.departments(department_id);


--
-- Name: job_history jhist_emp_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.job_history
    ADD CONSTRAINT jhist_emp_fk FOREIGN KEY (employee_id) REFERENCES hr.employees(employee_id);


--
-- Name: job_history jhist_job_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.job_history
    ADD CONSTRAINT jhist_job_fk FOREIGN KEY (job_id) REFERENCES hr.jobs(job_id);


--
-- Name: locations loc_c_id_fk; Type: FK CONSTRAINT; Schema: hr; Owner: hr
--

ALTER TABLE ONLY hr.locations
    ADD CONSTRAINT loc_c_id_fk FOREIGN KEY (country_id) REFERENCES hr.countries(country_id);


--
-- PostgreSQL database dump complete
--

