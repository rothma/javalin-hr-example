# Javalin Examples with HR Database
This repository serves as an example project, that illustrates the 
Web-Development with the Javalin micro-framework with access to a
relational database (PostgreSQL in this case).

## Project branches
The project contains multiple branches that reflect different types of database
access and view strategies. please checkout these branches for consultation of the
given implementations:

1. branch 'main'/'jdbc' connects to db via pure JDBC-Connection and implements a homegrown ConnectionPool.
2. branch 'jpa' uses hibernate as O/R-Mapper.
3. branch 'jpa-vue' uses hibernate as O/R-Mapper, exposes data as JSON and vue.js as client-side-rendered UI

## Database
The postgres variant of the HR databse used by the application code resides in
src/main/sql as a PostgreSQL Dump file (directly usable with pg_restore).
Prerequisite is a database and a user named 'hr' that will be owner of
the newly created hr schema.

